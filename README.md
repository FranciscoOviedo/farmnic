# FARM NIC
## _Aplicación móvil que muestra información de localización de farmacias_

Aplicación móvil que muestra en un mapa la ubicación y datos adicionales, de farmacias.

## Tecnologías

Entre algunas de las tecnologías usadas encontramos:

- [Android Studio](https://developer.android.com/studio) - Entorno de desarrollo que facilita la creación de aplicaciones móviles para android.
- [Java](https://www.java.com/es/) - Lenguaje orientado a objetos muy utilizado. Es el lenguaje predeterminado para desarrollo móvil en la plataforma android.

![1614738187546](/uploads/297f711db1a551107642cdf8665ab157/1614738187546.png)

![1614738187582](/uploads/ba1c9bd6dd85816378f946c0e8896b54/1614738187582.png)

![1614738187601](/uploads/6728d5c6b5d957dfa6b58d2129eb61f8/1614738187601.jpg)

![1614738187619](/uploads/5ab48abef16ae929345f8fa480cbf7e8/1614738187619.png)
