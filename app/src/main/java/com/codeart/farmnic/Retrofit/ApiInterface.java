package com.codeart.farmnic.Retrofit;

import com.codeart.farmnic.Modelo.Farmacia;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {
    @GET("farmacia")
    Call<List<Farmacia>> listFarmacia();

    @GET("farmacia/{id}")
    Call<Farmacia> listFarmaciaId(@Path("id") int idFarmacia);

    @POST("farmacia")
    Call<Farmacia> createFarmacia(@Body Farmacia farmacia);
}
