package com.codeart.farmnic.Modelo;

import com.google.android.gms.maps.model.Marker;
import com.google.gson.annotations.SerializedName;

public class Farmacia {
    private Marker mMarcador;
    @SerializedName("id")
    private int mId;
    @SerializedName("nombre")
    private String mNombre;
    @SerializedName("telefono")
    private int mTelefono;
    @SerializedName("direccion")
    private String mDireccion;
    @SerializedName("longitud")
    private float mLongitud;
    @SerializedName("latitud")
    private float mLatitud;
    @SerializedName("usuario_id")
    private int mUsuarioId;
    //TODO: Ver con que datos representar el Schedule

    public Farmacia(Marker marcador, String nombre, int telefono, String direccion){
        this.setmMarcador(marcador);
        this.setmNombre(nombre);
        this.setmDireccion(direccion);
        this.setmTelefono(telefono);
    }

    public Farmacia(Marker marcador, int id, String nombre, int telefono, String direccion, float longitud, float latitud, int usuarioId){
        this.setmMarcador(marcador);
        this.setmId(id);
        this.setmNombre(nombre);
        this.setmTelefono(telefono);
        this.setmDireccion(direccion);
        this.setmLongitud(longitud);
        this.setmLatitud(latitud);
        this.setmUsuarioId(usuarioId);
    }

    public Farmacia(Marker marcador, Farmacia farmacia){
        this.setmMarcador(marcador);
        this.setmId(farmacia.getmId());
        this.setmNombre(farmacia.getmNombre());
        this.setmTelefono(farmacia.getmTelefono());
        this.setmDireccion(farmacia.getmDireccion());
        this.setmLongitud(farmacia.getmLongitud());
        this.setmLatitud(farmacia.getmLatitud());
        this.setmUsuarioId(farmacia.getmUsuarioId());
    }

    public Marker getmMarcador(){
        return mMarcador;
    }

    public void setmMarcador(Marker mMarcador) {
        this.mMarcador = mMarcador;
    }

    public String getmNombre() {
        return mNombre;
    }

    public void setmNombre(String mNombre) {
        this.mNombre = mNombre;
    }

    public String getmDireccion() {
        return mDireccion;
    }

    public void setmDireccion(String mDireccion) {
        this.mDireccion = mDireccion;
    }

    public int getmTelefono() {
        return mTelefono;
    }

    public void setmTelefono(int mTelefono) {
        this.mTelefono = mTelefono;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public float getmLongitud() {
        return mLongitud;
    }

    public void setmLongitud(float mLongitud) {
        this.mLongitud = mLongitud;
    }

    public float getmLatitud() {
        return mLatitud;
    }

    public void setmLatitud(float mLatitud) {
        this.mLatitud = mLatitud;
    }

    public int getmUsuarioId() {
        return mUsuarioId;
    }

    public void setmUsuarioId(int mUsuarioId) {
        this.mUsuarioId = mUsuarioId;
    }
}
