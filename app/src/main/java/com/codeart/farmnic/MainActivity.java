package com.codeart.farmnic;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.codeart.farmnic.Modelo.Farmacia;
import com.codeart.farmnic.Retrofit.ApiFarmNic;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private GoogleMap mMap; //El mapa
    private Toolbar mToolbar; //Toolbar del activity
    private MaterialSearchView mSearchView; //Search view (Parte que permite buscar)
    private ArrayList<Farmacia> mFarmacias = new ArrayList<>(); // Arraylist de todas las farmacias
    private NestedScrollView mBottomSheetLayout; //Layout del bottom sheet
    private BottomSheetBehavior mBottomSheetBehaviour; //El comportamiento del bottom sheet
    private static final int REQUEST_LOCATION_PERMISSION = 99; // Para pedir permiso de la localizacion

    //Componentes del BottomSheet
    private Button btn_call;
    private Button btn_route;
    private TextView tv_title;
    private TextView tv_position;
    private TextView tv_phone_number;
    private TextView tv_schedule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Bind (Find view by id)
        bindComponents();

        //----------- Encontrar fragmento de mapa y cargar -------------
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapMain);
        mapFragment.getMapAsync(this);
    }


    private void bindComponents(){
        //Bind el Layout y Behaviour del bottom sheet
        mBottomSheetLayout = findViewById(R.id.bottomSheetLayout);
        mBottomSheetBehaviour = BottomSheetBehavior.from(mBottomSheetLayout);
        mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_HIDDEN);

        //Binding del toolbar
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        //Binding el Material SearchView
        mSearchView = findViewById(R.id.searchview);


        //Componentes del BottomSheet
         btn_call = findViewById(R.id.btn_call);
         btn_route = findViewById(R.id.btn_route);
         tv_title = findViewById(R.id.tv_title);
         tv_position = findViewById(R.id.tv_position);
         tv_phone_number = findViewById(R.id.tv_phone_number);
         tv_schedule = findViewById(R.id.tv_schedule);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem item = menu.findItem(R.id.menu_search);
        mSearchView.setMenuItem(item);
        String[] temp = new String[10];
        temp[0]="Farmacia 1";
        temp[1]="Farmacia 2";
        temp[2]="Farmacia 3";
        temp[3]="Farmacia 4";
        temp[4]="Farmacia 5";
        temp[5]="Farmacia 6";
        temp[6]="Farmacia 7";
        temp[7]="Farmacia 8";
        temp[8]="Farmacia 9";
        temp[9]="Farmacia 10";
        mSearchView.setSuggestions(temp);
        mSearchView.clearFocus();
        return true;
    }

    //Para cargar el mapa
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Cargar datos de localizaciones
        loadData();

        //Activar Geolocalizacion
        enableMyLocation();

        //Enviar el contexto para el click de los marcadores
        googleMap.setOnMarkerClickListener(this);
    }

    //Hacer que se expanda o minimize el bottomSheet...
    public void toggleBottomSheet(){
        mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    //Hacer que se expanda o minimize el bottomSheet...
    public void loadInfoBottomSheet(Farmacia farmacia){
        mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);
        tv_title.setText(farmacia.getmNombre());
        tv_position.setText(farmacia.getmDireccion());
        tv_phone_number.setText(String.valueOf(farmacia.getmTelefono()));
        final String numeroTelefono = String.valueOf(farmacia.getmTelefono());

        //Poner la funcionalidad del boton de llamadas (call) para abrir la aplicacion de llamadas con el numero ya puesto
        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "tel:" + numeroTelefono;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(uri));
                startActivity(intent);
            }
        });

        //TODO: Cargar el Schedule porque eso no esta para nada correcto
    }

    //Cargar datos de los marcadores... Por ahorita sera sincronico y sin acceder a base de datos... pero hay que ver como hacer esto...
    public void loadData(){
        LatLng tempLatLng;
        Marker tempMarker;

        //Solo es para probar la serializacion con GSON
        //Farmacia farmacia = new Farmacia(1,"Farmax",21234567,"De arriba para abajo",15.01f,45.12f,1);

        Call<List<Farmacia>> call = ApiFarmNic.instance().listFarmacia();
        call.enqueue(new Callback<List<Farmacia>>() {
            @Override
            public void onResponse(Call<List<Farmacia>> call, Response<List<Farmacia>> response) {
                for (Farmacia farmacia  : response.body()){
                    LatLng tempLatLng;
                    Marker tempMarker;
                    tempLatLng = new LatLng(farmacia.getmLatitud(), farmacia.getmLongitud());
                    tempMarker = mMap.addMarker(new MarkerOptions().position(tempLatLng));
                    mFarmacias.add(new Farmacia(tempMarker, farmacia));
                    Log.d("Response", String.valueOf(farmacia.getmLatitud()));
                }
            }

            @Override
            public void onFailure(Call<List<Farmacia>> call, Throwable t) {

            }
        });

        //TODO: Cargar datos de una web api
        tempLatLng = new LatLng(12.1021193, -86.2645674);
        tempMarker =(mMap.addMarker(new MarkerOptions().position(tempLatLng)));
        mFarmacias.add(new Farmacia(tempMarker,"UNIVERSAL MARKETING ASSOCIATES SA", 22773025, "Villa fontana del club terraza 150 varas al oeste. M/I"));
/*
        tempLatLng = new LatLng(12.1473855, -86.242299);
        tempMarker =(mMap.addMarker(new MarkerOptions().position(tempLatLng)));
        mFarmacias.add(new Farmacia(tempMarker,"FARMA VALUE (BELLO HORIZONTE)", 18013276, "Rotonda Bello Horizonte, 1 1/2 cuadra al este., Managua, Nicaragua"));

        tempLatLng = new LatLng(12.1344832, -86.2540458);
        tempMarker =(mMap.addMarker(new MarkerOptions().position(tempLatLng)));
        mFarmacias.add(new Farmacia(tempMarker,"FARMA VALUE (ALTAMIRA)", 18013276, "Banpro Altamira 1 Cuadra al Sur, Managua, Nicaragua"));

        tempLatLng = new LatLng(12.1313236, -86.2560036);
        tempMarker =(mMap.addMarker(new MarkerOptions().position(tempLatLng)));
        mFarmacias.add(new Farmacia(tempMarker,"FARMA VALUE (CAMINO DE ORIENTE)", 18013276, "Camino Oriente, Managua, Nicaragua"));

        tempLatLng = new LatLng(12.1400676, -86.3016139);
        tempMarker =(mMap.addMarker(new MarkerOptions().position(tempLatLng)));
        mFarmacias.add(new Farmacia(tempMarker,"FARMA VALUE (PLAZA ESPAÑA)", 18013276, "Rotonda El Güegüense, 1/2 cuadra al oeste Pista Benjamin Zeledon, Managua, Nicaragua"));
*/
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tempLatLng, 12));
    }

    //Habilitar localizacion del usuario
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION);
        }
    }

    //Callback de pedir el permiso de GeoLocalizacion
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // Chequear si el permiso de localizacion esta disponible de ser asi se habilita la capa de informacion de localizacion
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                if (grantResults.length > 0
                        && grantResults[0]
                        == PackageManager.PERMISSION_GRANTED) {
                    enableMyLocation();
                    break;
                }
        }
    }

    //CallBack cuando se clickea un marcador
    @Override
    public boolean onMarkerClick(Marker marker) {
        //Toast.makeText(this,"Marcador: " + marker.getTitle(), Toast.LENGTH_LONG).show();
        for (Farmacia farmacia : mFarmacias) {
            if(marker.equals(farmacia.getmMarcador())){
                loadInfoBottomSheet(farmacia);
            }
        }
        return false;
    }
}
